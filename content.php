<section class="row">

    <article class="col-md-12">

        </div>
        <div class="panel panel-primary">

            <div class="panel-heading">

                <hr>
                <h3 class="panel-title"> Каталог </h3>
            </div>
            <div class="panel-body" id="katalog">

                <table class="table table-bordered table-striped table-hover ">

                    <tr class="success">
                        <th class="col-md-1"> id</th>
                        <th class="col-md-2"> Название</th>
                        <th class="col-md-9"> Описание</th>
                        </th>
                    </tr>
                    <?php foreach ($data as $i): ?>
                        <tr data-toggle="modal" data-target="#modal_form">
                            <td class="col-md-1 id">
                                <?= $i[id]; ?>
                            </td>
                            <td class="col-md-1 name">
                                <?= $i[name]; ?>
                            </td>
                            <td class="col-md-1 name_descr">
                                <?= $i[name_descr]; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <div class="panel-footer">
            </div>
        </div>


    </article>
    <?php

    include 'modal_form.php';
    ?>


</section><!-- end <section class="row"> -->