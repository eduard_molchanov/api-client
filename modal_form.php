
<!-- Modal -->
<div class="modal fadebs-example-modal-lg " id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class=" modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"> Детальное описание - id :
                    <span id="id" class="label label-danger"></span></h4>
            </div>
            <div class="modal-body">
                <!-- info modal-->
                <div class="jumbotron">

                    <hr>

                    <form class="form-horizontal" role="form" id="f_add_page" action="" method="post">

                        <div class="form-group">
                            <label for="a2" class="col-sm-2 control-label"> название </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control float-sm-left" id="name" placeholder="название " name="name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="a3" class="col-sm-2 control-label"> описание </label>
                            <div class="col-sm-10">
                                <textarea class="form-control float-sm-left" rows="4" placeholder="описание " id="name_descr"
                                          name="name_descr"></textarea>
                            </div>
                        </div>


                    </form>
                </div><!-- div class="jumbotron" -->
                <!-- info modal-->
            </div>

        </div>
    </div>
</div><!-- Modal -->
